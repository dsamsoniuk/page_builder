<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
// use Symfony\Component\HttpFoundation\Session\Session;


class MainController extends AbstractController
{

    /**
      * @Route("/", name="main_index")
      */
    public function index()
    {
        $number = random_int(0, 100);
   
        return $this->render('index.html.twig', [
            'number' => $number,
        ]);
        // return new Response(
        //     '<html><body>Lucky number: '.$number.'</body></html>'
        // );
    }
    /**
      * @Route("/testy", name="test")
      */
    public function test()
    {

        echo 'TESTY';
        $number = random_int(0, 100);

        return $this->render('index.html.twig', [
            'number' => $number,
        ]);
  
    }
}