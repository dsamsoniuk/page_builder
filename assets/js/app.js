/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// require('bootstrap');
window.$ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
// const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
// require('../css/global.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
// require the JavaScript

window.callAjax = function(url, callback, async, data){
    var async = async ? async : false;
    var ax_data = {
        async: async,
        type: "POST",
        url: url,
        data: data ? data : '{ }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: callback
    };
    $.ajax(ax_data);
};

window.redirect = function(url) {
    window.location.replace(url);
};

window.alertAction = function (action, text) {
    var alert_box = $('#alert-info')
    if (!alert_box.length) {
        console.log('brak alerta')
        return;
    }
    var height = alert_box.css('height').replace('px','');
    var position = '0px';
    if (action == 'show') {
        position = '-3px';
        // Schowaj po chwili
        setTimeout(function(){
            alertAction('hide');
        }, 5000);
    }
    else if (action == 'hide') {
        position = '-'+ (height + 3) + 'px';
    }
    alert_box.html(text);
    alert_box.animate({
        'top': position
    },'slow');
};