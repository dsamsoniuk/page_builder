
window.nextStage = function() {
    var data  = JSON.stringify(page.generateStructure());

    callAjax('/set/widget', function(res){
        console.log(res);
        redirect('/stage/view');

    }, false, data );
};

window.widgets = (function(){

    var panel = $('#main_panel');
    var page_block = $('#page_block');
    var screen_width = screen.width;
    
    function movePanel(margin_left, margin_right) {
        panel.animate({
            left: margin_left,
            right: margin_right
        }, 100);
        panel.css({'right':margin_right});
        panel.css({'left':margin_left});
    };

    function changeSide() {
        this.place = (this.place == 'right') ? 'left' : 'right';
        this.show();
    };

    function selectBlock(btn) {
        selected_block = $(btn);
        $(btn).closest('.block').addClass('selected_block');
    };

    function unselectBlock() {
        page_block.find('.selected_block').removeClass('selected_block');
        selected_block = false;
    };

    function show(btn) {
        if (this.place == 'left') {
            $('#sample_view').css({'float':'right'});
            $('#sample_list').css({'float':'left'});
            movePanel('0px', 'auto');
        }
        else if (this.place == 'right') {
            $('#sample_view').css({'float':'left'});
            $('#sample_list').css({'float':'right'});
            movePanel('auto','0px');
        }
        if (btn) {
            unselectBlock();
            selectBlock(btn);
        }
    };

    function hide() {
        var panel_width = panel.css('width').replace('px','');
        if (this.place == 'left') {
            movePanel( '-' + panel_width + 'px', 'auto' );
        }
        if (this.place == 'right') {
            movePanel('auto', '-' + panel_width + 'px' );
        } 
        unselectBlock();
    };

    function putElement(btn) {
        var element = $(btn).closest('.sample_row');
        var element_html = element.find('.sample_html').html();
        var element_id = element.attr('sample_id');
        
        var box_element = '<span class="widget" widget_id="'+element_id+'" >' +
        element_html +
        '<span><button onclick="widgets.removeElement(this)">x</button></span>' + ' \
            </span>';
        selected_block.find('.widget_list').append(box_element);
    };

    function removeElement(btn){
        $(btn).closest('.widget').remove();
    };

    function viewElement(btn) {
        var html = $(btn).closest('.sample_row').find('.sample_html').html();
        $('#sample_view').find('.sample-view-content').html(html);
        $('#sample_view').show();
        $('.sample_row').removeClass('sample_row_hover');
        $(btn).closest('.sample_row').addClass('sample_row_hover');
        widgets.show();

    };

    function viewElementHide() {
        $('.sample_row').removeClass('sample_row_hover');
        $('#sample_view').hide();
        widgets.show();
    };

    return {
        place: 'left',
        show : show,
        hide : hide,
        changeSide : changeSide,
        putElement : putElement,
        removeElement : removeElement,
        viewElement : viewElement,
        viewElementHide : viewElementHide,
    };
})();

window.page = (function(params){
    
    var page_box = document.getElementById('page_block');
    var block_row = page_box.getElementsByClassName('block_row')[0].cloneNode(true);
    var block = block_row.getElementsByClassName('block')[0].cloneNode(true);
    
    init();

    function init() {
        // Generate page structure
        page_box.innerHTML = '';
        for (var i=0; i<params.length; i++) {
            block_row.innerHTML= '';
            var row = block_row.cloneNode(true);
            var block_list = [];
            var new_block = false;

            for (var j=0; j<params[i].length; j++) {
                new_block = block.cloneNode(true);
                new_block.style.width = params[i][j]['width'] + '%';
                //new_block.style['min-height'] = params[i][j]['height'] + '%';
                row.appendChild(new_block)
            }
            page_box.appendChild(row);
        }
    };
    
    function generateStructure() {
        var main = document.getElementById('page_block');
        var parents = main.getElementsByClassName('block_row');
        var structure = [];
        var widget_block = false;
        for (var i=0; i<parents.length; i++) {
            widget_block = parents[i].getElementsByClassName('block');
            var row = [];
            for (var j=0; j<widget_block.length; j++) {
                var widgets_list = widget_block[j].getElementsByClassName('widget');
                var d = [];
                for (var k=0; k<widgets_list.length; k++) {
                    var widget_id = widgets_list[k].getAttribute('widget_id');
                    d.push( widget_id );
                }
                row.push(d);
            }
            structure.push(row)
        }
        // console.log(structure);
        return structure;
    };


    return {
        generateStructure: generateStructure
    };

})(parameters );

