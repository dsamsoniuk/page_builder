<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;

use App\Entity\User;
use App\Entity\Element;
use App\Entity\Framework;

use App\Service\ElementService;

class ViewController extends AbstractController
{

    /**
      * @Route("/stage/view", name="view_index")
      * Description : Dodawanie viewow do podstrony
      */
    public function index(ElementService $elementService)
    {

        $session = new Session();
  
        $user      = $this->get('security.token_storage')->getToken()->getUser();
        $em        = $this->getDoctrine()->getManager();

        
  
        // $current_framework = $em->getRepository(Framework::class)->find(1);
        // $elements = $em->getRepository(Element::class)->findBy(['framework' => $current_framework]);
        
        // foreach ($elements as $e) {
        //     $elements_html[] = ['html' => $e->getHtml(), 'id' => $e->getId()];
        // }

        $data = $elementService->getAllParamsPage(1);

        // dump($data);
        // die;
        return $this->render('stages/view.html.twig', [
            'params' =>$data,
            // 'current_framework' => $current_framework,
            // 'elements' => $elements,
            // 'elements_html' => json_encode($elements_html)
        ]);


// echo json_encode($arr);
        // file_put_contents("../../public/web_temp/test.html","Hello World. Testing!");
        $data = $elementService->getAllParamsPage(1);
        // $data['element_html'] = json_encode($html_eles);
        $data_encode = [];

        foreach ($data['element_html'] as &$e) {
            // dump($e);
            $e = htmlspecialchars($e);
        }
        // die;
        foreach ($data as $d => $c) {
            $data_encode[$d] = json_encode($c);
        }


        return $this->render('stages/view.html.twig', $data_encode );
     
    }
}