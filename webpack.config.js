var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')
    .cleanupOutputBeforeBuild()

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('bootstrapjs', './assets/js/lib/bootstrap.min.js')
    .addEntry('slim', './assets/js/lib/slim.min.js')
    .addEntry('popper', './assets/js/lib/popper.min.js')
    .addEntry('jquery', './assets/js/lib/jquery.js')

    .addEntry('stage/bootstrap/template', './assets/js/stage/bootstrap_framework/template.js')
    .addEntry('stage/bootstrap/widget', './assets/js/stage/bootstrap_framework/widget.js')
    .addEntry('stage/bootstrap/view', './assets/js/stage/bootstrap_framework/view.js')
    //.addEntry('page2', './assets/js/page2.js')
    .addStyleEntry('bootstrapcss', './assets/css/bootstrap.min.css')
    .addStyleEntry('cover', './assets/css/cover.css')
    .addStyleEntry('frameworks/bootstrap/bootstrap-custom', './assets/css/stages/bootstrap_framework/bootstrap-custom.css')
    .addStyleEntry('frameworks/bootstrap/widget', './assets/css/stages/bootstrap_framework/widget.css')
    .addStyleEntry('stage-default', './assets/css/stage-default.css')
    // .addStyleEntry('bootstrapcsstheme', './assets/css/bootstrap-theme.min.css')

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
