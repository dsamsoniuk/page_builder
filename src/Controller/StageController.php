<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;

use App\Entity\User;
use App\Entity\Element;
use App\Entity\Framework;

use App\Service\ElementService;

class StageController extends AbstractController
{

    /**
      * @Route("/create/{page}", name="create_stage")
      */
    public function index($page)
    {

        // $session = new Session();
  
        // $user      = $this->get('security.token_storage')->getToken()->getUser();
        // $em        = $this->getDoctrine()->getManager();
        $page_path = 'stage/template';
        if ($page == 'template') {
            $page_path = '/stage/template';
        }
        if ($page == 'widget') {
            $page_path = '/stage/widget';
        }
        if ($page == 'view') {
            $page_path = '/stage/view';
        }

        return $this->render('stage_view.html.twig', [
            'page' => $page_path
        ] );
     
    }
}