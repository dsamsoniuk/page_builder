<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FrameworkRepository")
 */
class Framework
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $lib_css;

    /**
     * @ORM\Column(type="text")
     */
    private $lib_js;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $state = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLibCss(): ?string
    {
        return $this->lib_css;
    }

    public function setLibCss(string $lib_css): self
    {
        $this->lib_css = $lib_css;

        return $this;
    }

    public function getLibJs(): ?string
    {
        return $this->lib_js;
    }

    public function setLibJs(string $lib_js): self
    {
        $this->lib_js = $lib_js;

        return $this;
    }

    public function getState(): ?array
    {
        return $this->state;
    }

    public function setState(?array $state): self
    {
        $this->state = $state;

        return $this;
    }
}
