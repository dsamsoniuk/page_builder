<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\User;


class TemplateController extends AbstractController
{

    /**
      * @Route("/stage/template", name="template_index")
      * Description : tworzenie szablonu strony
      */
    public function index()
    {


        $user      = $this->get('security.token_storage')->getToken()->getUser();
        $em        = $this->getDoctrine()->getManager();

        $repo      = $em->getRepository(User::class)->find(2);


        //  dump($user);die;

        return $this->render('stages/template.html.twig', [
        ]);
     
    }
}