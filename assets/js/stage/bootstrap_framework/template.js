
window.nextStage = function () {
    var data  = JSON.stringify(block.getStructure());
    callAjax('/set/template', function(res){
        console.log(res);
        redirect('/stage/widget');
        // alertAction('show', 'Zapisane jedziemy dalje')
        // TODO : tutaj redirect do etapu 2
    }, false,  data  );
};

window.block = (function(){

    var block = document.getElementsByClassName('block')[0].cloneNode(true);
    var width_screen = document.body.clientWidth;
    var height_screen = document.body.clientHeight;

    var config = {
        resize: {
            max: 100, // 100%
            min: 20, // 20%
            count: 10 // przesuniecie rozmiaru o 10%
        }
    };

    function findIndexElement(list, element) {
        var index = 0, i=0;
        for(i=0;i<list.children.length;i++) {
            if (list.children[i] === element) {
                index= i;
                break;
            }
        }
        return (i == list.children.length && !index) ? false : index;
    };

    function findBlock(list, index) {
        return list[index] || false;
    };

    function getSizeElement(element) {
        var width_per = '';
        // var height_per = '';
        if (element.style.width) {
            width_per = element.style.width.replace('%','');
            // height_per = element.style.height.replace('%','');
        } else {
            var width = element.offsetWidth;
            // var height = element.offsetHeight;
            width_per = Math.ceil( ( (width * 100) / width_screen) * 100)  / 100;
            // height_per = Math.floor((((height * 100) / height_screen) * 1000 )) / 1000 ;
        }
        return {
            'width': parseInt(width_per), // W procentach
            // 'height': height_per,  // W procentach
        };
    };

    function resizeElement(element, size) {
        // console.log(size)
        element.style.width = size.width + '%';
        // element.style.height = (size.height/2) + '%';
        return element;
    };

    function split(btn) {
        // wyciagnij index tego bloku
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);
        if (index !== false) {
            var size_block = getSizeElement(current_block);

            size_block.width = size_block.width/2;
            // TODO : tutaj blokada na zbyt mala szerokosc
            var block_1 = resizeElement(block.cloneNode(true), size_block);
            var block_2 = resizeElement(block.cloneNode(true), size_block);
            parent.replaceChild(block_1, parent.children[index]);
            parent.insertBefore(block_2, parent.children[index]);
        }
    };

    function join(btn, direction) { // polacz bloki
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);

        if (index === false) {
            return;
        }
        var children = parent.getElementsByClassName('block');
        var block_left = false;

        if (direction == 'left') {
            block_left = findBlock(children, index - 1);
        }
        else if (direction == 'right') {
            block_left = findBlock(children, index + 1);
        }

        if (block_left) {
            var size_block_cur = getSizeElement(current_block);
            var size_block_left = getSizeElement(block_left);
            var new_size = size_block_cur.width + size_block_left.width;
            resizeElement(current_block, {width : new_size})
            block_left.remove();
        }
    };

    function resizeWidth(btn, direction) {
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);

        if (index === false) {
            return;
        }

        var children = parent.getElementsByClassName('block');
        var block_dir = false;
        if (direction == 'left') {
            block_dir = findBlock(children, index - 1);
        }
        else if (direction == 'right') {
            block_dir = findBlock(children, index + 1);
        }
        if (block_dir) {
            var size_block_cur = getSizeElement(current_block);
            var size_block_dir = getSizeElement(block_dir);
            size_block_cur.width += config.resize.count;
            size_block_dir.width -= config.resize.count;
            if (size_block_dir.width > config.resize.max || size_block_dir.width < config.resize.min) {
                return;
            }
            else if (size_block_cur.width > config.resize.max || size_block_cur.width < config.resize.min) {
                return;
            }
            resizeElement(current_block, size_block_cur);
            resizeElement(block_dir, size_block_dir);
        }
    };

    function addRow(direction) {
        var main = document.getElementById('page_block');
        var parents = main.getElementsByClassName('block_row');
        var new_block_row = false;
        if (direction == "bottom") {
            var index = parents.length - 1
            new_block_row = parents[ index ].cloneNode(true);
            main.appendChild(new_block_row);
        }
        if (direction == "top") {
            new_block_row = parents[ 0 ].cloneNode(true);
            main.insertBefore(new_block_row, parents[ 0 ]);
        }
    };

    function getStructure() {
        var main = document.getElementById('page_block')
        var parents = main.getElementsByClassName('block_row');
        var structure = [];
        for (var i=0; i<parents.length; i++) {
            var children = parents[i].getElementsByClassName('block');
            var row = [];

            for (var j=0; j<children.length; j++) {
                row.push({
                    width: children[j].style.width || '100%',
                    height: children[j].style.height ||'100%',
                });
            }
            structure.push(row);
        }
    };

    function getStructure() {
        var main = document.getElementById('page_block')
        var parents = main.getElementsByClassName('block_row');
        var structure = [];
        for (var i=0; i<parents.length; i++) {
            var children = parents[i].getElementsByClassName('block');
            var row = [];

            for (var j=0; j<children.length; j++) {
                row.push({
                    width: children[j].style.width || '100%',
                    height: children[j].style.height ||'100%',
                });
            }
        structure.push(row);    
        }
        return structure;
    };

    return {
        split : split,
        join : join,
        resizeWidth : resizeWidth,
        addRow : addRow,
        getStructure : getStructure,
    };
})();
