<?php

// src/Service/MessageGenerator.php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Element;
use App\Entity\Framework;
use Symfony\Component\HttpFoundation\Session\Session;

class ElementService
{
    public function __construct(EntityManagerInterface $entityManager, \Twig_Environment $templating)
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;

    }

    public function getIds($list)
    {
        $em     = $this->entityManager;
        $ids    = [];

        $this->recursive($list);

        foreach ($this->elements as $e) {
            $ids[] = intval($e['id']);
        }

        $current_framework = $em->getRepository(Framework::class)->find(1);

        $elements = $em->getRepository(Element::class)->findBy(['framework' => $current_framework, 'id' => $ids]);

        return $elements;
    }
    /**
     * Przeszukaj tabele w poszukiwaniu elementow
     */
    public $elements = [];
    public function recursive($list) {
        if (isset($list['elements'])) {
            $this->elements = array_merge($this->elements, $list['elements']);
            return;
        }
        foreach ($list as $e) {
            $this->recursive($e);
        }
    }
    /**
     * Wygeneruj wszystkie parametry z sesji czyli szablon, widgety i ich rozmieszczenie
     */
    public function getAllParamsPage($framework_id=1) {

        $session    = new Session();
        $em         = $this->entityManager;
        $current_framework = $em->getRepository(Framework::class)->find($framework_id);
        $params     = ($session->has('param_page')) ? $session->get('param_page') : [];
        $widgets   = ($session->has('widget_page')) ? $session->get('widget_page') : [];
        $ids        = [];

        foreach ($widgets as $v) {
            foreach ($v as $element) {
                foreach ($element as $id) {
                    if (!in_array($id, $ids)) {
                        $ids[]  = $id;
                    }
                }
            }
        }

        $ele_db = $em->getRepository(Element::class)->findBy(['framework' => $current_framework, 'id' => $ids]);
        $elements = [];

        foreach ($ele_db as $v) {
            $elements[ $v->getId() ] = $v;
        }

        for ($i=0; $i<count($params); $i++) {
            for ($j=0; $j<count($params[$i]); $j++) {
                if (empty( $widgets[$i][$j] )) {
                    continue;
                }
                if (!isset($params[$i][$j]['widgets'])) {
                    $params[$i][$j]['widgets'] = [];
                }
                for ($k=0; $k<count($widgets[$i][$j]); $k++) {
                    $widget_id = $widgets[$i][$j][$k];
                    $params[$i][$j]['widgets'][] = $elements[ $widget_id ]->getHtml();
                }
            }
        }

        return $params;
        // $session = new Session();

        // $em  = $this->entityManager;
        // $current_framework = $em->getRepository(Framework::class)->find($framework_id);
        // $elements = $em->getRepository(Element::class)->findBy(['framework' => $current_framework]);
        // $data = [
        //     'params'    => $session->get('param_page'),
        //     'elements'  => $session->get('widget_page'),
        // ];
        // $html_eles = [];
        // $ele_objects = $this->getIds( $session->get('widget_page') );
        // foreach ($ele_objects as $e) {
        //     $html_eles[$e->getId()] = $e->getHtml();
        // }
        // $data['element_html'] = $html_eles;

        // return $data;
    }

    // public function generateHtmlPage($params, $framework_id) {
    //     $em  = $this->entityManager;

    //     $current_framework = $em->getRepository(Framework::class)->find($framework_id);
    //     $framework_name = $current_framework->getName();

    //     // dump($params);
    //     // die;
    //     $html = '';
    //     foreach ($params['params'] as $i => $c) {
    //         $html .= $this->recursiveGenerate($c, $params['elements'][$i], $params['element_html'], $framework_name);
    //     }
    //     // dump($html);
    //     // die;
    //     return $html;
    // }

    // private function recursiveGenerate($param, $element_pos, $ele_html, $fw_name) {

    //     $html = '';

    //     if ( is_string($param[0]) ) {
    //         $html .= $this->templating->render('page/content_bootstrap.html.twig', [
    //             'elements' => $element_pos,
    //             'elements_html' => $ele_html
    //         ]);
    //         return $html;
    //     } 
    //     if (empty($param)) {
    //         $html .= $this->templating->render('page/close_tag_'.$fw_name.'.html.twig', []);
    //     }

    //     foreach ($param as $p => $c) {

    //         $ele = ( isset($element_pos['elements']) ) ? $element_pos['elements'] : ( isset($element_pos[$p]) ? $element_pos[$p] : []);

    //         $html .= $this->templating->render('page/open_tag_'.$fw_name.'.html.twig', [
    //             'params' => is_string($c[0]) ? $c : []
    //         ]);

    //         $html .= $this->recursiveGenerate($c, $ele, $ele_html, $fw_name);
    //         $html .= $this->templating->render('page/close_tag_'.$fw_name.'.html.twig', []);
    //     }

    //     return $html;
    // }

}