<?php
// src/Controller/LuckyController.php
namespace App\Controller\RPC;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use App\Service\ElementService;

use ZipArchive;


class StageController extends AbstractController
{

    /**
      * @Route("/set/template", name="set_template_index")
      * Description : Dodawanie widgetow do podstrony
      */
    public function template(Request $request)
    {
        $session = new Session();

        $data_post  = $request->getContent();
        $data_post  = str_replace("\n","",$data_post);
        $data_post  = str_replace("\t","",$data_post);

        $params = json_decode($data_post, true);
        $data = [];
        
        foreach ($params as $param) {
          $d = [];
          foreach ($param as $p) {
            $attr = [];
            foreach ($p as $i => $v) {
              $attr[ $i ] = intval( $v );
            }
            $d[] = $attr;
          }
          $data[] = $d;
        }

        $session->set('param_page', $data);
        $session->set('widget_page', []);

        return new JsonResponse( array("result" => "success",  'data' => $data_post) );
    }

    /**
      * @Route("/set/widget", name="set_widget_index")
      * Description : Dodawanie widgetow do podstrony
      */
      public function widget(Request $request)
      {
        $data_post  = $request->getContent();
        $data_post  = str_replace("\n","",$data_post);
        $data_post  = str_replace("\t","",$data_post);

        $session = new Session();

        $elements = json_decode($data_post, true);

        $session->set('widget_page', $elements);

        return new JsonResponse( array("result" => "success" ) );
      }

      /**
       * @Route("/generate", name="set_page")
       * Wygeneruj gotowa stronę
       */
      public function generatePage(ElementService $elementService) {

        $user       = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == 'anon.') {
          return new JsonResponse( array("result" => "fail", "info" => "Zaloguj sie") );
        }
        $session    = new Session();
        $fileSystem = new Filesystem();

        $data       = $elementService->getAllParamsPage(1);

        $main_dir   = '../public/user_temp/';
        $user_dir   = 'user_'.$user->getId();

        if ( !$fileSystem->exists($main_dir.$user_dir) ) {

          try {
              $fileSystem->mkdir($main_dir.'/'.$user_dir, 0777);
              $fileSystem->mkdir($main_dir.$user_dir.'/template');
              $fileSystem->mkdir($main_dir.$user_dir.'/template/js');
              $fileSystem->mkdir($main_dir.$user_dir.'/template/css');
          } catch (IOExceptionInterface $exception) {
              echo "An error occurred while creating your directory at ".$exception->getPath();
          }
          
        } else {
          // Usun wszystkie pliki ktore znajduja sie w katalogu uzytkownika
          $files = glob($main_dir.$user_dir.'/template/{,.}*'); // get all file names
          foreach ($files as $file) { // iterate files
            if(is_file($file) ) {
              unlink($file); // delete file
            }
          }
        }

        // $content = $this->prepareBodyPage($elementService);
        // TEMPL : BOOTSTRAP

        // $html = $this->render('page/page_bootstrap.html.twig', ["body" => $content] );

        // $fileSystem->touch($main_dir.$user_dir."/template/index.html");
        // $fileSystem->dumpFile($main_dir.$user_dir."/template/index.html", $html);

        $html = $this->render('page/page_bootstrap.html.twig', [
          'params' => $data
        ] );

        $fileSystem->touch($main_dir.$user_dir."/template/index.html");
        $fileSystem->dumpFile($main_dir.$user_dir."/template/index.html", $html->getContent());
        // $fileSystem->dumpFile($main_dir.$user_dir."/template/test.html", $html);


        // BOOTSTRAP
        $fileSystem->copy('build/bootstrapjs.js', $main_dir.$user_dir."/template/js/bootstrap.js");
        $fileSystem->copy('build/jquery.js', $main_dir.$user_dir."/template/js/jquery.js");
        $fileSystem->copy('build/bootstrapcss.css', $main_dir.$user_dir."/template/css/bootstrap.css");
        $fileSystem->copy('build/bootstrap-custom-stage.css', $main_dir.$user_dir."/template/css/bootstrap-custom-stage.css");
        // $fileSystem->copy('build/bootstrapcsstheme.css', $main_dir.$user_dir."/template/css/bootstrapcsstheme.css");


        // file_put_contents($main_dir.$user_dir."/template/test.html", "Hello World. Testing!");

        //TODO::  TUTAJ PROBLEM JEST PO STRONIE WINDOWSA NIE MA JAK UPRAWNIENIA PRZYDZIELIC NA SERWERZE NAJWYZEJ
        // $this->zipDir($main_dir.$user_dir.'/page.zip', $main_dir.$user_dir.'/template/*');
        
        // Zwroc gotowy zip do uzytkownika
        // $response = new BinaryFileResponse($main_dir.$user_dir."/template/test.html");
        // $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        // return $response;



      return new JsonResponse( array("result" => "success", "info" => "Pliki wygenerowane") );

      }



      // TODO : tutaj co sie sypia pozwolenia na odczyt
      private function zipDir($zip_name, $path_files){

        $zip = new ZipArchive();
        // $zip_name = time().".zip"; // Zip name
        $files = glob($path_files); // get all file names

        $zip->open($zip_name,  ZipArchive::CREATE);
        foreach ($files as $file) {
          echo $path = $file;
          if(file_exists($path)){
          $zip->addFromString(basename($path),  file_get_contents($path));//---This is main function  
          }
          else{
          echo"file does not exist";
          }
        }
        $zip->close();
      }



}