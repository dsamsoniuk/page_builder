/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/build/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/stage/bootstrap_framework/template.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/stage/bootstrap_framework/template.js":
/*!*********************************************************!*\
  !*** ./assets/js/stage/bootstrap_framework/template.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {


window.nextStage = function () {
    var data = JSON.stringify(block.getStructure());
    callAjax('/set/template', function (res) {
        console.log(res);
        redirect('/stage/widget');
        // alertAction('show', 'Zapisane jedziemy dalje')
        // TODO : tutaj redirect do etapu 2
    }, false, data);
};

window.block = function () {

    var block = document.getElementsByClassName('block')[0].cloneNode(true);
    var width_screen = document.body.clientWidth;
    var height_screen = document.body.clientHeight;

    var config = {
        resize: {
            max: 100, // 100%
            min: 20, // 20%
            count: 10 // przesuniecie rozmiaru o 10%
        }
    };

    function findIndexElement(list, element) {
        var index = 0,
            i = 0;
        for (i = 0; i < list.children.length; i++) {
            if (list.children[i] === element) {
                index = i;
                break;
            }
        }
        return i == list.children.length && !index ? false : index;
    };

    function findBlock(list, index) {
        return list[index] || false;
    };

    function getSizeElement(element) {
        var width_per = '';
        // var height_per = '';
        if (element.style.width) {
            width_per = element.style.width.replace('%', '');
            // height_per = element.style.height.replace('%','');
        } else {
            var width = element.offsetWidth;
            // var height = element.offsetHeight;
            width_per = Math.ceil(width * 100 / width_screen * 100) / 100;
            // height_per = Math.floor((((height * 100) / height_screen) * 1000 )) / 1000 ;
        }
        return {
            'width': parseInt(width_per) // W procentach
            // 'height': height_per,  // W procentach
        };
    };

    function resizeElement(element, size) {
        // console.log(size)
        element.style.width = size.width + '%';
        // element.style.height = (size.height/2) + '%';
        return element;
    };

    function split(btn) {
        // wyciagnij index tego bloku
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);
        if (index !== false) {
            var size_block = getSizeElement(current_block);

            size_block.width = size_block.width / 2;
            // TODO : tutaj blokada na zbyt mala szerokosc
            var block_1 = resizeElement(block.cloneNode(true), size_block);
            var block_2 = resizeElement(block.cloneNode(true), size_block);
            parent.replaceChild(block_1, parent.children[index]);
            parent.insertBefore(block_2, parent.children[index]);
        }
    };

    function join(btn, direction) {
        // polacz bloki
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);

        if (index === false) {
            return;
        }
        var children = parent.getElementsByClassName('block');
        var block_left = false;

        if (direction == 'left') {
            block_left = findBlock(children, index - 1);
        } else if (direction == 'right') {
            block_left = findBlock(children, index + 1);
        }

        if (block_left) {
            var size_block_cur = getSizeElement(current_block);
            var size_block_left = getSizeElement(block_left);
            var new_size = size_block_cur.width + size_block_left.width;
            resizeElement(current_block, { width: new_size });
            block_left.remove();
        }
    };

    function resizeWidth(btn, direction) {
        var parent = btn.closest('.block_row');
        var current_block = btn.closest('.block');
        var index = findIndexElement(parent, current_block);

        if (index === false) {
            return;
        }

        var children = parent.getElementsByClassName('block');
        var block_dir = false;
        if (direction == 'left') {
            block_dir = findBlock(children, index - 1);
        } else if (direction == 'right') {
            block_dir = findBlock(children, index + 1);
        }
        if (block_dir) {
            var size_block_cur = getSizeElement(current_block);
            var size_block_dir = getSizeElement(block_dir);
            size_block_cur.width += config.resize.count;
            size_block_dir.width -= config.resize.count;
            if (size_block_dir.width > config.resize.max || size_block_dir.width < config.resize.min) {
                return;
            } else if (size_block_cur.width > config.resize.max || size_block_cur.width < config.resize.min) {
                return;
            }
            resizeElement(current_block, size_block_cur);
            resizeElement(block_dir, size_block_dir);
        }
    };

    function addRow(direction) {
        var main = document.getElementById('page_block');
        var parents = main.getElementsByClassName('block_row');
        var new_block_row = false;
        if (direction == "bottom") {
            var index = parents.length - 1;
            new_block_row = parents[index].cloneNode(true);
            main.appendChild(new_block_row);
        }
        if (direction == "top") {
            new_block_row = parents[0].cloneNode(true);
            main.insertBefore(new_block_row, parents[0]);
        }
    };

    function getStructure() {
        var main = document.getElementById('page_block');
        var parents = main.getElementsByClassName('block_row');
        var structure = [];
        for (var i = 0; i < parents.length; i++) {
            var children = parents[i].getElementsByClassName('block');
            var row = [];

            for (var j = 0; j < children.length; j++) {
                row.push({
                    width: children[j].style.width || '100%',
                    height: children[j].style.height || '100%'
                });
            }
            structure.push(row);
        }
    };

    function getStructure() {
        var main = document.getElementById('page_block');
        var parents = main.getElementsByClassName('block_row');
        var structure = [];
        for (var i = 0; i < parents.length; i++) {
            var children = parents[i].getElementsByClassName('block');
            var row = [];

            for (var j = 0; j < children.length; j++) {
                row.push({
                    width: children[j].style.width || '100%',
                    height: children[j].style.height || '100%'
                });
            }
            structure.push(row);
        }
        return structure;
    };

    return {
        split: split,
        join: join,
        resizeWidth: resizeWidth,
        addRow: addRow,
        getStructure: getStructure
    };
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYzA2NDE3MzQyOWM3N2UzNzU0MmUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3N0YWdlL2Jvb3RzdHJhcF9mcmFtZXdvcmsvdGVtcGxhdGUuanMiXSwibmFtZXMiOlsid2luZG93IiwibmV4dFN0YWdlIiwiZGF0YSIsIkpTT04iLCJzdHJpbmdpZnkiLCJibG9jayIsImdldFN0cnVjdHVyZSIsImNhbGxBamF4IiwicmVzIiwiY29uc29sZSIsImxvZyIsInJlZGlyZWN0IiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50c0J5Q2xhc3NOYW1lIiwiY2xvbmVOb2RlIiwid2lkdGhfc2NyZWVuIiwiYm9keSIsImNsaWVudFdpZHRoIiwiaGVpZ2h0X3NjcmVlbiIsImNsaWVudEhlaWdodCIsImNvbmZpZyIsInJlc2l6ZSIsIm1heCIsIm1pbiIsImNvdW50IiwiZmluZEluZGV4RWxlbWVudCIsImxpc3QiLCJlbGVtZW50IiwiaW5kZXgiLCJpIiwiY2hpbGRyZW4iLCJsZW5ndGgiLCJmaW5kQmxvY2siLCJnZXRTaXplRWxlbWVudCIsIndpZHRoX3BlciIsInN0eWxlIiwid2lkdGgiLCJyZXBsYWNlIiwib2Zmc2V0V2lkdGgiLCJNYXRoIiwiY2VpbCIsInBhcnNlSW50IiwicmVzaXplRWxlbWVudCIsInNpemUiLCJzcGxpdCIsImJ0biIsInBhcmVudCIsImNsb3Nlc3QiLCJjdXJyZW50X2Jsb2NrIiwic2l6ZV9ibG9jayIsImJsb2NrXzEiLCJibG9ja18yIiwicmVwbGFjZUNoaWxkIiwiaW5zZXJ0QmVmb3JlIiwiam9pbiIsImRpcmVjdGlvbiIsImJsb2NrX2xlZnQiLCJzaXplX2Jsb2NrX2N1ciIsInNpemVfYmxvY2tfbGVmdCIsIm5ld19zaXplIiwicmVtb3ZlIiwicmVzaXplV2lkdGgiLCJibG9ja19kaXIiLCJzaXplX2Jsb2NrX2RpciIsImFkZFJvdyIsIm1haW4iLCJnZXRFbGVtZW50QnlJZCIsInBhcmVudHMiLCJuZXdfYmxvY2tfcm93IiwiYXBwZW5kQ2hpbGQiLCJzdHJ1Y3R1cmUiLCJyb3ciLCJqIiwicHVzaCIsImhlaWdodCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzVEQUEsT0FBT0MsU0FBUCxHQUFtQixZQUFZO0FBQzNCLFFBQUlDLE9BQVFDLEtBQUtDLFNBQUwsQ0FBZUMsTUFBTUMsWUFBTixFQUFmLENBQVo7QUFDQUMsYUFBUyxlQUFULEVBQTBCLFVBQVNDLEdBQVQsRUFBYTtBQUNuQ0MsZ0JBQVFDLEdBQVIsQ0FBWUYsR0FBWjtBQUNBRyxpQkFBUyxlQUFUO0FBQ0E7QUFDQTtBQUNILEtBTEQsRUFLRyxLQUxILEVBS1dULElBTFg7QUFNSCxDQVJEOztBQVVBRixPQUFPSyxLQUFQLEdBQWdCLFlBQVU7O0FBRXRCLFFBQUlBLFFBQVFPLFNBQVNDLHNCQUFULENBQWdDLE9BQWhDLEVBQXlDLENBQXpDLEVBQTRDQyxTQUE1QyxDQUFzRCxJQUF0RCxDQUFaO0FBQ0EsUUFBSUMsZUFBZUgsU0FBU0ksSUFBVCxDQUFjQyxXQUFqQztBQUNBLFFBQUlDLGdCQUFnQk4sU0FBU0ksSUFBVCxDQUFjRyxZQUFsQzs7QUFFQSxRQUFJQyxTQUFTO0FBQ1RDLGdCQUFRO0FBQ0pDLGlCQUFLLEdBREQsRUFDTTtBQUNWQyxpQkFBSyxFQUZELEVBRUs7QUFDVEMsbUJBQU8sRUFISCxDQUdNO0FBSE47QUFEQyxLQUFiOztBQVFBLGFBQVNDLGdCQUFULENBQTBCQyxJQUExQixFQUFnQ0MsT0FBaEMsRUFBeUM7QUFDckMsWUFBSUMsUUFBUSxDQUFaO0FBQUEsWUFBZUMsSUFBRSxDQUFqQjtBQUNBLGFBQUlBLElBQUUsQ0FBTixFQUFRQSxJQUFFSCxLQUFLSSxRQUFMLENBQWNDLE1BQXhCLEVBQStCRixHQUEvQixFQUFvQztBQUNoQyxnQkFBSUgsS0FBS0ksUUFBTCxDQUFjRCxDQUFkLE1BQXFCRixPQUF6QixFQUFrQztBQUM5QkMsd0JBQU9DLENBQVA7QUFDQTtBQUNIO0FBQ0o7QUFDRCxlQUFRQSxLQUFLSCxLQUFLSSxRQUFMLENBQWNDLE1BQW5CLElBQTZCLENBQUNILEtBQS9CLEdBQXdDLEtBQXhDLEdBQWdEQSxLQUF2RDtBQUNIOztBQUVELGFBQVNJLFNBQVQsQ0FBbUJOLElBQW5CLEVBQXlCRSxLQUF6QixFQUFnQztBQUM1QixlQUFPRixLQUFLRSxLQUFMLEtBQWUsS0FBdEI7QUFDSDs7QUFFRCxhQUFTSyxjQUFULENBQXdCTixPQUF4QixFQUFpQztBQUM3QixZQUFJTyxZQUFZLEVBQWhCO0FBQ0E7QUFDQSxZQUFJUCxRQUFRUSxLQUFSLENBQWNDLEtBQWxCLEVBQXlCO0FBQ3JCRix3QkFBWVAsUUFBUVEsS0FBUixDQUFjQyxLQUFkLENBQW9CQyxPQUFwQixDQUE0QixHQUE1QixFQUFnQyxFQUFoQyxDQUFaO0FBQ0E7QUFDSCxTQUhELE1BR087QUFDSCxnQkFBSUQsUUFBUVQsUUFBUVcsV0FBcEI7QUFDQTtBQUNBSix3QkFBWUssS0FBS0MsSUFBTCxDQUFjSixRQUFRLEdBQVQsR0FBZ0JyQixZQUFsQixHQUFrQyxHQUE3QyxJQUFxRCxHQUFqRTtBQUNBO0FBQ0g7QUFDRCxlQUFPO0FBQ0gscUJBQVMwQixTQUFTUCxTQUFULENBRE4sQ0FDMkI7QUFDOUI7QUFGRyxTQUFQO0FBSUg7O0FBRUQsYUFBU1EsYUFBVCxDQUF1QmYsT0FBdkIsRUFBZ0NnQixJQUFoQyxFQUFzQztBQUNsQztBQUNBaEIsZ0JBQVFRLEtBQVIsQ0FBY0MsS0FBZCxHQUFzQk8sS0FBS1AsS0FBTCxHQUFhLEdBQW5DO0FBQ0E7QUFDQSxlQUFPVCxPQUFQO0FBQ0g7O0FBRUQsYUFBU2lCLEtBQVQsQ0FBZUMsR0FBZixFQUFvQjtBQUNoQjtBQUNBLFlBQUlDLFNBQVNELElBQUlFLE9BQUosQ0FBWSxZQUFaLENBQWI7QUFDQSxZQUFJQyxnQkFBZ0JILElBQUlFLE9BQUosQ0FBWSxRQUFaLENBQXBCO0FBQ0EsWUFBSW5CLFFBQVFILGlCQUFpQnFCLE1BQWpCLEVBQXlCRSxhQUF6QixDQUFaO0FBQ0EsWUFBSXBCLFVBQVUsS0FBZCxFQUFxQjtBQUNqQixnQkFBSXFCLGFBQWFoQixlQUFlZSxhQUFmLENBQWpCOztBQUVBQyx1QkFBV2IsS0FBWCxHQUFtQmEsV0FBV2IsS0FBWCxHQUFpQixDQUFwQztBQUNBO0FBQ0EsZ0JBQUljLFVBQVVSLGNBQWNyQyxNQUFNUyxTQUFOLENBQWdCLElBQWhCLENBQWQsRUFBcUNtQyxVQUFyQyxDQUFkO0FBQ0EsZ0JBQUlFLFVBQVVULGNBQWNyQyxNQUFNUyxTQUFOLENBQWdCLElBQWhCLENBQWQsRUFBcUNtQyxVQUFyQyxDQUFkO0FBQ0FILG1CQUFPTSxZQUFQLENBQW9CRixPQUFwQixFQUE2QkosT0FBT2hCLFFBQVAsQ0FBZ0JGLEtBQWhCLENBQTdCO0FBQ0FrQixtQkFBT08sWUFBUCxDQUFvQkYsT0FBcEIsRUFBNkJMLE9BQU9oQixRQUFQLENBQWdCRixLQUFoQixDQUE3QjtBQUNIO0FBQ0o7O0FBRUQsYUFBUzBCLElBQVQsQ0FBY1QsR0FBZCxFQUFtQlUsU0FBbkIsRUFBOEI7QUFBRTtBQUM1QixZQUFJVCxTQUFTRCxJQUFJRSxPQUFKLENBQVksWUFBWixDQUFiO0FBQ0EsWUFBSUMsZ0JBQWdCSCxJQUFJRSxPQUFKLENBQVksUUFBWixDQUFwQjtBQUNBLFlBQUluQixRQUFRSCxpQkFBaUJxQixNQUFqQixFQUF5QkUsYUFBekIsQ0FBWjs7QUFFQSxZQUFJcEIsVUFBVSxLQUFkLEVBQXFCO0FBQ2pCO0FBQ0g7QUFDRCxZQUFJRSxXQUFXZ0IsT0FBT2pDLHNCQUFQLENBQThCLE9BQTlCLENBQWY7QUFDQSxZQUFJMkMsYUFBYSxLQUFqQjs7QUFFQSxZQUFJRCxhQUFhLE1BQWpCLEVBQXlCO0FBQ3JCQyx5QkFBYXhCLFVBQVVGLFFBQVYsRUFBb0JGLFFBQVEsQ0FBNUIsQ0FBYjtBQUNILFNBRkQsTUFHSyxJQUFJMkIsYUFBYSxPQUFqQixFQUEwQjtBQUMzQkMseUJBQWF4QixVQUFVRixRQUFWLEVBQW9CRixRQUFRLENBQTVCLENBQWI7QUFDSDs7QUFFRCxZQUFJNEIsVUFBSixFQUFnQjtBQUNaLGdCQUFJQyxpQkFBaUJ4QixlQUFlZSxhQUFmLENBQXJCO0FBQ0EsZ0JBQUlVLGtCQUFrQnpCLGVBQWV1QixVQUFmLENBQXRCO0FBQ0EsZ0JBQUlHLFdBQVdGLGVBQWVyQixLQUFmLEdBQXVCc0IsZ0JBQWdCdEIsS0FBdEQ7QUFDQU0sMEJBQWNNLGFBQWQsRUFBNkIsRUFBQ1osT0FBUXVCLFFBQVQsRUFBN0I7QUFDQUgsdUJBQVdJLE1BQVg7QUFDSDtBQUNKOztBQUVELGFBQVNDLFdBQVQsQ0FBcUJoQixHQUFyQixFQUEwQlUsU0FBMUIsRUFBcUM7QUFDakMsWUFBSVQsU0FBU0QsSUFBSUUsT0FBSixDQUFZLFlBQVosQ0FBYjtBQUNBLFlBQUlDLGdCQUFnQkgsSUFBSUUsT0FBSixDQUFZLFFBQVosQ0FBcEI7QUFDQSxZQUFJbkIsUUFBUUgsaUJBQWlCcUIsTUFBakIsRUFBeUJFLGFBQXpCLENBQVo7O0FBRUEsWUFBSXBCLFVBQVUsS0FBZCxFQUFxQjtBQUNqQjtBQUNIOztBQUVELFlBQUlFLFdBQVdnQixPQUFPakMsc0JBQVAsQ0FBOEIsT0FBOUIsQ0FBZjtBQUNBLFlBQUlpRCxZQUFZLEtBQWhCO0FBQ0EsWUFBSVAsYUFBYSxNQUFqQixFQUF5QjtBQUNyQk8sd0JBQVk5QixVQUFVRixRQUFWLEVBQW9CRixRQUFRLENBQTVCLENBQVo7QUFDSCxTQUZELE1BR0ssSUFBSTJCLGFBQWEsT0FBakIsRUFBMEI7QUFDM0JPLHdCQUFZOUIsVUFBVUYsUUFBVixFQUFvQkYsUUFBUSxDQUE1QixDQUFaO0FBQ0g7QUFDRCxZQUFJa0MsU0FBSixFQUFlO0FBQ1gsZ0JBQUlMLGlCQUFpQnhCLGVBQWVlLGFBQWYsQ0FBckI7QUFDQSxnQkFBSWUsaUJBQWlCOUIsZUFBZTZCLFNBQWYsQ0FBckI7QUFDQUwsMkJBQWVyQixLQUFmLElBQXdCaEIsT0FBT0MsTUFBUCxDQUFjRyxLQUF0QztBQUNBdUMsMkJBQWUzQixLQUFmLElBQXdCaEIsT0FBT0MsTUFBUCxDQUFjRyxLQUF0QztBQUNBLGdCQUFJdUMsZUFBZTNCLEtBQWYsR0FBdUJoQixPQUFPQyxNQUFQLENBQWNDLEdBQXJDLElBQTRDeUMsZUFBZTNCLEtBQWYsR0FBdUJoQixPQUFPQyxNQUFQLENBQWNFLEdBQXJGLEVBQTBGO0FBQ3RGO0FBQ0gsYUFGRCxNQUdLLElBQUlrQyxlQUFlckIsS0FBZixHQUF1QmhCLE9BQU9DLE1BQVAsQ0FBY0MsR0FBckMsSUFBNENtQyxlQUFlckIsS0FBZixHQUF1QmhCLE9BQU9DLE1BQVAsQ0FBY0UsR0FBckYsRUFBMEY7QUFDM0Y7QUFDSDtBQUNEbUIsMEJBQWNNLGFBQWQsRUFBNkJTLGNBQTdCO0FBQ0FmLDBCQUFjb0IsU0FBZCxFQUF5QkMsY0FBekI7QUFDSDtBQUNKOztBQUVELGFBQVNDLE1BQVQsQ0FBZ0JULFNBQWhCLEVBQTJCO0FBQ3ZCLFlBQUlVLE9BQU9yRCxTQUFTc0QsY0FBVCxDQUF3QixZQUF4QixDQUFYO0FBQ0EsWUFBSUMsVUFBVUYsS0FBS3BELHNCQUFMLENBQTRCLFdBQTVCLENBQWQ7QUFDQSxZQUFJdUQsZ0JBQWdCLEtBQXBCO0FBQ0EsWUFBSWIsYUFBYSxRQUFqQixFQUEyQjtBQUN2QixnQkFBSTNCLFFBQVF1QyxRQUFRcEMsTUFBUixHQUFpQixDQUE3QjtBQUNBcUMsNEJBQWdCRCxRQUFTdkMsS0FBVCxFQUFpQmQsU0FBakIsQ0FBMkIsSUFBM0IsQ0FBaEI7QUFDQW1ELGlCQUFLSSxXQUFMLENBQWlCRCxhQUFqQjtBQUNIO0FBQ0QsWUFBSWIsYUFBYSxLQUFqQixFQUF3QjtBQUNwQmEsNEJBQWdCRCxRQUFTLENBQVQsRUFBYXJELFNBQWIsQ0FBdUIsSUFBdkIsQ0FBaEI7QUFDQW1ELGlCQUFLWixZQUFMLENBQWtCZSxhQUFsQixFQUFpQ0QsUUFBUyxDQUFULENBQWpDO0FBQ0g7QUFDSjs7QUFFRCxhQUFTN0QsWUFBVCxHQUF3QjtBQUNwQixZQUFJMkQsT0FBT3JELFNBQVNzRCxjQUFULENBQXdCLFlBQXhCLENBQVg7QUFDQSxZQUFJQyxVQUFVRixLQUFLcEQsc0JBQUwsQ0FBNEIsV0FBNUIsQ0FBZDtBQUNBLFlBQUl5RCxZQUFZLEVBQWhCO0FBQ0EsYUFBSyxJQUFJekMsSUFBRSxDQUFYLEVBQWNBLElBQUVzQyxRQUFRcEMsTUFBeEIsRUFBZ0NGLEdBQWhDLEVBQXFDO0FBQ2pDLGdCQUFJQyxXQUFXcUMsUUFBUXRDLENBQVIsRUFBV2hCLHNCQUFYLENBQWtDLE9BQWxDLENBQWY7QUFDQSxnQkFBSTBELE1BQU0sRUFBVjs7QUFFQSxpQkFBSyxJQUFJQyxJQUFFLENBQVgsRUFBY0EsSUFBRTFDLFNBQVNDLE1BQXpCLEVBQWlDeUMsR0FBakMsRUFBc0M7QUFDbENELG9CQUFJRSxJQUFKLENBQVM7QUFDTHJDLDJCQUFPTixTQUFTMEMsQ0FBVCxFQUFZckMsS0FBWixDQUFrQkMsS0FBbEIsSUFBMkIsTUFEN0I7QUFFTHNDLDRCQUFRNUMsU0FBUzBDLENBQVQsRUFBWXJDLEtBQVosQ0FBa0J1QyxNQUFsQixJQUEyQjtBQUY5QixpQkFBVDtBQUlIO0FBQ0RKLHNCQUFVRyxJQUFWLENBQWVGLEdBQWY7QUFDSDtBQUNKOztBQUVELGFBQVNqRSxZQUFULEdBQXdCO0FBQ3BCLFlBQUkyRCxPQUFPckQsU0FBU3NELGNBQVQsQ0FBd0IsWUFBeEIsQ0FBWDtBQUNBLFlBQUlDLFVBQVVGLEtBQUtwRCxzQkFBTCxDQUE0QixXQUE1QixDQUFkO0FBQ0EsWUFBSXlELFlBQVksRUFBaEI7QUFDQSxhQUFLLElBQUl6QyxJQUFFLENBQVgsRUFBY0EsSUFBRXNDLFFBQVFwQyxNQUF4QixFQUFnQ0YsR0FBaEMsRUFBcUM7QUFDakMsZ0JBQUlDLFdBQVdxQyxRQUFRdEMsQ0FBUixFQUFXaEIsc0JBQVgsQ0FBa0MsT0FBbEMsQ0FBZjtBQUNBLGdCQUFJMEQsTUFBTSxFQUFWOztBQUVBLGlCQUFLLElBQUlDLElBQUUsQ0FBWCxFQUFjQSxJQUFFMUMsU0FBU0MsTUFBekIsRUFBaUN5QyxHQUFqQyxFQUFzQztBQUNsQ0Qsb0JBQUlFLElBQUosQ0FBUztBQUNMckMsMkJBQU9OLFNBQVMwQyxDQUFULEVBQVlyQyxLQUFaLENBQWtCQyxLQUFsQixJQUEyQixNQUQ3QjtBQUVMc0MsNEJBQVE1QyxTQUFTMEMsQ0FBVCxFQUFZckMsS0FBWixDQUFrQnVDLE1BQWxCLElBQTJCO0FBRjlCLGlCQUFUO0FBSUg7QUFDTEosc0JBQVVHLElBQVYsQ0FBZUYsR0FBZjtBQUNDO0FBQ0QsZUFBT0QsU0FBUDtBQUNIOztBQUVELFdBQU87QUFDSDFCLGVBQVFBLEtBREw7QUFFSFUsY0FBT0EsSUFGSjtBQUdITyxxQkFBY0EsV0FIWDtBQUlIRyxnQkFBU0EsTUFKTjtBQUtIMUQsc0JBQWVBO0FBTFosS0FBUDtBQU9ILENBOUxjLEVBQWYsQyIsImZpbGUiOiJzdGFnZS9ib290c3RyYXAvdGVtcGxhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvYnVpbGQvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2Fzc2V0cy9qcy9zdGFnZS9ib290c3RyYXBfZnJhbWV3b3JrL3RlbXBsYXRlLmpzXCIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGMwNjQxNzM0MjljNzdlMzc1NDJlIiwiXHJcbndpbmRvdy5uZXh0U3RhZ2UgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgZGF0YSAgPSBKU09OLnN0cmluZ2lmeShibG9jay5nZXRTdHJ1Y3R1cmUoKSk7XHJcbiAgICBjYWxsQWpheCgnL3NldC90ZW1wbGF0ZScsIGZ1bmN0aW9uKHJlcyl7XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzKTtcclxuICAgICAgICByZWRpcmVjdCgnL3N0YWdlL3dpZGdldCcpO1xyXG4gICAgICAgIC8vIGFsZXJ0QWN0aW9uKCdzaG93JywgJ1phcGlzYW5lIGplZHppZW15IGRhbGplJylcclxuICAgICAgICAvLyBUT0RPIDogdHV0YWogcmVkaXJlY3QgZG8gZXRhcHUgMlxyXG4gICAgfSwgZmFsc2UsICBkYXRhICApO1xyXG59O1xyXG5cclxud2luZG93LmJsb2NrID0gKGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgdmFyIGJsb2NrID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYmxvY2snKVswXS5jbG9uZU5vZGUodHJ1ZSk7XHJcbiAgICB2YXIgd2lkdGhfc2NyZWVuID0gZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aDtcclxuICAgIHZhciBoZWlnaHRfc2NyZWVuID0gZG9jdW1lbnQuYm9keS5jbGllbnRIZWlnaHQ7XHJcblxyXG4gICAgdmFyIGNvbmZpZyA9IHtcclxuICAgICAgICByZXNpemU6IHtcclxuICAgICAgICAgICAgbWF4OiAxMDAsIC8vIDEwMCVcclxuICAgICAgICAgICAgbWluOiAyMCwgLy8gMjAlXHJcbiAgICAgICAgICAgIGNvdW50OiAxMCAvLyBwcnplc3VuaWVjaWUgcm96bWlhcnUgbyAxMCVcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIGZpbmRJbmRleEVsZW1lbnQobGlzdCwgZWxlbWVudCkge1xyXG4gICAgICAgIHZhciBpbmRleCA9IDAsIGk9MDtcclxuICAgICAgICBmb3IoaT0wO2k8bGlzdC5jaGlsZHJlbi5sZW5ndGg7aSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChsaXN0LmNoaWxkcmVuW2ldID09PSBlbGVtZW50KSB7XHJcbiAgICAgICAgICAgICAgICBpbmRleD0gaTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAoaSA9PSBsaXN0LmNoaWxkcmVuLmxlbmd0aCAmJiAhaW5kZXgpID8gZmFsc2UgOiBpbmRleDtcclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gZmluZEJsb2NrKGxpc3QsIGluZGV4KSB7XHJcbiAgICAgICAgcmV0dXJuIGxpc3RbaW5kZXhdIHx8IGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBmdW5jdGlvbiBnZXRTaXplRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIHdpZHRoX3BlciA9ICcnO1xyXG4gICAgICAgIC8vIHZhciBoZWlnaHRfcGVyID0gJyc7XHJcbiAgICAgICAgaWYgKGVsZW1lbnQuc3R5bGUud2lkdGgpIHtcclxuICAgICAgICAgICAgd2lkdGhfcGVyID0gZWxlbWVudC5zdHlsZS53aWR0aC5yZXBsYWNlKCclJywnJyk7XHJcbiAgICAgICAgICAgIC8vIGhlaWdodF9wZXIgPSBlbGVtZW50LnN0eWxlLmhlaWdodC5yZXBsYWNlKCclJywnJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmFyIHdpZHRoID0gZWxlbWVudC5vZmZzZXRXaWR0aDtcclxuICAgICAgICAgICAgLy8gdmFyIGhlaWdodCA9IGVsZW1lbnQub2Zmc2V0SGVpZ2h0O1xyXG4gICAgICAgICAgICB3aWR0aF9wZXIgPSBNYXRoLmNlaWwoICggKHdpZHRoICogMTAwKSAvIHdpZHRoX3NjcmVlbikgKiAxMDApICAvIDEwMDtcclxuICAgICAgICAgICAgLy8gaGVpZ2h0X3BlciA9IE1hdGguZmxvb3IoKCgoaGVpZ2h0ICogMTAwKSAvIGhlaWdodF9zY3JlZW4pICogMTAwMCApKSAvIDEwMDAgO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAnd2lkdGgnOiBwYXJzZUludCh3aWR0aF9wZXIpLCAvLyBXIHByb2NlbnRhY2hcclxuICAgICAgICAgICAgLy8gJ2hlaWdodCc6IGhlaWdodF9wZXIsICAvLyBXIHByb2NlbnRhY2hcclxuICAgICAgICB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBmdW5jdGlvbiByZXNpemVFbGVtZW50KGVsZW1lbnQsIHNpemUpIHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhzaXplKVxyXG4gICAgICAgIGVsZW1lbnQuc3R5bGUud2lkdGggPSBzaXplLndpZHRoICsgJyUnO1xyXG4gICAgICAgIC8vIGVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gKHNpemUuaGVpZ2h0LzIpICsgJyUnO1xyXG4gICAgICAgIHJldHVybiBlbGVtZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICBmdW5jdGlvbiBzcGxpdChidG4pIHtcclxuICAgICAgICAvLyB3eWNpYWduaWogaW5kZXggdGVnbyBibG9rdVxyXG4gICAgICAgIHZhciBwYXJlbnQgPSBidG4uY2xvc2VzdCgnLmJsb2NrX3JvdycpO1xyXG4gICAgICAgIHZhciBjdXJyZW50X2Jsb2NrID0gYnRuLmNsb3Nlc3QoJy5ibG9jaycpO1xyXG4gICAgICAgIHZhciBpbmRleCA9IGZpbmRJbmRleEVsZW1lbnQocGFyZW50LCBjdXJyZW50X2Jsb2NrKTtcclxuICAgICAgICBpZiAoaW5kZXggIT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHZhciBzaXplX2Jsb2NrID0gZ2V0U2l6ZUVsZW1lbnQoY3VycmVudF9ibG9jayk7XHJcblxyXG4gICAgICAgICAgICBzaXplX2Jsb2NrLndpZHRoID0gc2l6ZV9ibG9jay53aWR0aC8yO1xyXG4gICAgICAgICAgICAvLyBUT0RPIDogdHV0YWogYmxva2FkYSBuYSB6Ynl0IG1hbGEgc3plcm9rb3NjXHJcbiAgICAgICAgICAgIHZhciBibG9ja18xID0gcmVzaXplRWxlbWVudChibG9jay5jbG9uZU5vZGUodHJ1ZSksIHNpemVfYmxvY2spO1xyXG4gICAgICAgICAgICB2YXIgYmxvY2tfMiA9IHJlc2l6ZUVsZW1lbnQoYmxvY2suY2xvbmVOb2RlKHRydWUpLCBzaXplX2Jsb2NrKTtcclxuICAgICAgICAgICAgcGFyZW50LnJlcGxhY2VDaGlsZChibG9ja18xLCBwYXJlbnQuY2hpbGRyZW5baW5kZXhdKTtcclxuICAgICAgICAgICAgcGFyZW50Lmluc2VydEJlZm9yZShibG9ja18yLCBwYXJlbnQuY2hpbGRyZW5baW5kZXhdKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIGpvaW4oYnRuLCBkaXJlY3Rpb24pIHsgLy8gcG9sYWN6IGJsb2tpXHJcbiAgICAgICAgdmFyIHBhcmVudCA9IGJ0bi5jbG9zZXN0KCcuYmxvY2tfcm93Jyk7XHJcbiAgICAgICAgdmFyIGN1cnJlbnRfYmxvY2sgPSBidG4uY2xvc2VzdCgnLmJsb2NrJyk7XHJcbiAgICAgICAgdmFyIGluZGV4ID0gZmluZEluZGV4RWxlbWVudChwYXJlbnQsIGN1cnJlbnRfYmxvY2spO1xyXG5cclxuICAgICAgICBpZiAoaW5kZXggPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gcGFyZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2Jsb2NrJyk7XHJcbiAgICAgICAgdmFyIGJsb2NrX2xlZnQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKGRpcmVjdGlvbiA9PSAnbGVmdCcpIHtcclxuICAgICAgICAgICAgYmxvY2tfbGVmdCA9IGZpbmRCbG9jayhjaGlsZHJlbiwgaW5kZXggLSAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoZGlyZWN0aW9uID09ICdyaWdodCcpIHtcclxuICAgICAgICAgICAgYmxvY2tfbGVmdCA9IGZpbmRCbG9jayhjaGlsZHJlbiwgaW5kZXggKyAxKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChibG9ja19sZWZ0KSB7XHJcbiAgICAgICAgICAgIHZhciBzaXplX2Jsb2NrX2N1ciA9IGdldFNpemVFbGVtZW50KGN1cnJlbnRfYmxvY2spO1xyXG4gICAgICAgICAgICB2YXIgc2l6ZV9ibG9ja19sZWZ0ID0gZ2V0U2l6ZUVsZW1lbnQoYmxvY2tfbGVmdCk7XHJcbiAgICAgICAgICAgIHZhciBuZXdfc2l6ZSA9IHNpemVfYmxvY2tfY3VyLndpZHRoICsgc2l6ZV9ibG9ja19sZWZ0LndpZHRoO1xyXG4gICAgICAgICAgICByZXNpemVFbGVtZW50KGN1cnJlbnRfYmxvY2ssIHt3aWR0aCA6IG5ld19zaXplfSlcclxuICAgICAgICAgICAgYmxvY2tfbGVmdC5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIHJlc2l6ZVdpZHRoKGJ0biwgZGlyZWN0aW9uKSB7XHJcbiAgICAgICAgdmFyIHBhcmVudCA9IGJ0bi5jbG9zZXN0KCcuYmxvY2tfcm93Jyk7XHJcbiAgICAgICAgdmFyIGN1cnJlbnRfYmxvY2sgPSBidG4uY2xvc2VzdCgnLmJsb2NrJyk7XHJcbiAgICAgICAgdmFyIGluZGV4ID0gZmluZEluZGV4RWxlbWVudChwYXJlbnQsIGN1cnJlbnRfYmxvY2spO1xyXG5cclxuICAgICAgICBpZiAoaW5kZXggPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBjaGlsZHJlbiA9IHBhcmVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdibG9jaycpO1xyXG4gICAgICAgIHZhciBibG9ja19kaXIgPSBmYWxzZTtcclxuICAgICAgICBpZiAoZGlyZWN0aW9uID09ICdsZWZ0Jykge1xyXG4gICAgICAgICAgICBibG9ja19kaXIgPSBmaW5kQmxvY2soY2hpbGRyZW4sIGluZGV4IC0gMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGRpcmVjdGlvbiA9PSAncmlnaHQnKSB7XHJcbiAgICAgICAgICAgIGJsb2NrX2RpciA9IGZpbmRCbG9jayhjaGlsZHJlbiwgaW5kZXggKyAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGJsb2NrX2Rpcikge1xyXG4gICAgICAgICAgICB2YXIgc2l6ZV9ibG9ja19jdXIgPSBnZXRTaXplRWxlbWVudChjdXJyZW50X2Jsb2NrKTtcclxuICAgICAgICAgICAgdmFyIHNpemVfYmxvY2tfZGlyID0gZ2V0U2l6ZUVsZW1lbnQoYmxvY2tfZGlyKTtcclxuICAgICAgICAgICAgc2l6ZV9ibG9ja19jdXIud2lkdGggKz0gY29uZmlnLnJlc2l6ZS5jb3VudDtcclxuICAgICAgICAgICAgc2l6ZV9ibG9ja19kaXIud2lkdGggLT0gY29uZmlnLnJlc2l6ZS5jb3VudDtcclxuICAgICAgICAgICAgaWYgKHNpemVfYmxvY2tfZGlyLndpZHRoID4gY29uZmlnLnJlc2l6ZS5tYXggfHwgc2l6ZV9ibG9ja19kaXIud2lkdGggPCBjb25maWcucmVzaXplLm1pbikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHNpemVfYmxvY2tfY3VyLndpZHRoID4gY29uZmlnLnJlc2l6ZS5tYXggfHwgc2l6ZV9ibG9ja19jdXIud2lkdGggPCBjb25maWcucmVzaXplLm1pbikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJlc2l6ZUVsZW1lbnQoY3VycmVudF9ibG9jaywgc2l6ZV9ibG9ja19jdXIpO1xyXG4gICAgICAgICAgICByZXNpemVFbGVtZW50KGJsb2NrX2Rpciwgc2l6ZV9ibG9ja19kaXIpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gYWRkUm93KGRpcmVjdGlvbikge1xyXG4gICAgICAgIHZhciBtYWluID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3BhZ2VfYmxvY2snKTtcclxuICAgICAgICB2YXIgcGFyZW50cyA9IG1haW4uZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYmxvY2tfcm93Jyk7XHJcbiAgICAgICAgdmFyIG5ld19ibG9ja19yb3cgPSBmYWxzZTtcclxuICAgICAgICBpZiAoZGlyZWN0aW9uID09IFwiYm90dG9tXCIpIHtcclxuICAgICAgICAgICAgdmFyIGluZGV4ID0gcGFyZW50cy5sZW5ndGggLSAxXHJcbiAgICAgICAgICAgIG5ld19ibG9ja19yb3cgPSBwYXJlbnRzWyBpbmRleCBdLmNsb25lTm9kZSh0cnVlKTtcclxuICAgICAgICAgICAgbWFpbi5hcHBlbmRDaGlsZChuZXdfYmxvY2tfcm93KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGRpcmVjdGlvbiA9PSBcInRvcFwiKSB7XHJcbiAgICAgICAgICAgIG5ld19ibG9ja19yb3cgPSBwYXJlbnRzWyAwIF0uY2xvbmVOb2RlKHRydWUpO1xyXG4gICAgICAgICAgICBtYWluLmluc2VydEJlZm9yZShuZXdfYmxvY2tfcm93LCBwYXJlbnRzWyAwIF0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gZ2V0U3RydWN0dXJlKCkge1xyXG4gICAgICAgIHZhciBtYWluID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3BhZ2VfYmxvY2snKVxyXG4gICAgICAgIHZhciBwYXJlbnRzID0gbWFpbi5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdibG9ja19yb3cnKTtcclxuICAgICAgICB2YXIgc3RydWN0dXJlID0gW107XHJcbiAgICAgICAgZm9yICh2YXIgaT0wOyBpPHBhcmVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIGNoaWxkcmVuID0gcGFyZW50c1tpXS5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdibG9jaycpO1xyXG4gICAgICAgICAgICB2YXIgcm93ID0gW107XHJcblxyXG4gICAgICAgICAgICBmb3IgKHZhciBqPTA7IGo8Y2hpbGRyZW4ubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogY2hpbGRyZW5bal0uc3R5bGUud2lkdGggfHwgJzEwMCUnLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogY2hpbGRyZW5bal0uc3R5bGUuaGVpZ2h0IHx8JzEwMCUnLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc3RydWN0dXJlLnB1c2gocm93KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIGdldFN0cnVjdHVyZSgpIHtcclxuICAgICAgICB2YXIgbWFpbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwYWdlX2Jsb2NrJylcclxuICAgICAgICB2YXIgcGFyZW50cyA9IG1haW4uZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYmxvY2tfcm93Jyk7XHJcbiAgICAgICAgdmFyIHN0cnVjdHVyZSA9IFtdO1xyXG4gICAgICAgIGZvciAodmFyIGk9MDsgaTxwYXJlbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciBjaGlsZHJlbiA9IHBhcmVudHNbaV0uZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYmxvY2snKTtcclxuICAgICAgICAgICAgdmFyIHJvdyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgZm9yICh2YXIgaj0wOyBqPGNoaWxkcmVuLmxlbmd0aDsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICByb3cucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGNoaWxkcmVuW2pdLnN0eWxlLndpZHRoIHx8ICcxMDAlJyxcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGNoaWxkcmVuW2pdLnN0eWxlLmhlaWdodCB8fCcxMDAlJyxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgc3RydWN0dXJlLnB1c2gocm93KTsgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzdHJ1Y3R1cmU7XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgc3BsaXQgOiBzcGxpdCxcclxuICAgICAgICBqb2luIDogam9pbixcclxuICAgICAgICByZXNpemVXaWR0aCA6IHJlc2l6ZVdpZHRoLFxyXG4gICAgICAgIGFkZFJvdyA6IGFkZFJvdyxcclxuICAgICAgICBnZXRTdHJ1Y3R1cmUgOiBnZXRTdHJ1Y3R1cmUsXHJcbiAgICB9O1xyXG59KSgpO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9hc3NldHMvanMvc3RhZ2UvYm9vdHN0cmFwX2ZyYW1ld29yay90ZW1wbGF0ZS5qcyJdLCJzb3VyY2VSb290IjoiIn0=