<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;

use App\Entity\User;
use App\Entity\Element;
use App\Entity\Framework;


class WidgetController extends AbstractController
{

    /**
      * @Route("/stage/widget", name="widget_index")
      * Description : Dodawanie widgetow do podstrony
      */
    public function index()
    {

        $session = new Session();
  
        $user      = $this->get('security.token_storage')->getToken()->getUser();
        $em        = $this->getDoctrine()->getManager();

        
        $current_framework = $em->getRepository(Framework::class)->find(1);
        $elements = $em->getRepository(Element::class)->findBy(['framework' => $current_framework]);
        foreach ($elements as $e) {
            $elements_html[] = ['html' => $e->getHtml(), 'id' => $e->getId()];
        }


        return $this->render('stages/widget.html.twig', [
            'params' => json_encode($session->get('param_page')),
            // 'current_framework' => $current_framework,
            'elements' => $elements,
            // 'elements_html' => json_encode($elements_html)
        ]);
     
    }
}